package com.android.ika.dolanyukfix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private EditText user, pass;
    private Button log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        log = findViewById(R.id.login);
        user = findViewById(R.id.user);
        pass = findViewById(R.id.pass);
    }

    public void btn_login(View view) {
        startActivity(new Intent(LoginActivity.this, MainMenu.class));
    }

    public void btn_signup(View view) {
        startActivity(new Intent(LoginActivity.this, SignUp.class));
    }

    public void login(View view) {
        final String nama = user.getText().toString();
        String word = pass.getText().toString();

        if (nama.equals("DOLAN") && word.equals("YUK")){
            Toast.makeText(this, "Login Success", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(LoginActivity.this, MainMenu.class);
            startActivity(intent);

        }else if (nama.equals("EAD") && word.equals("MOBILE")){
            Toast.makeText(this, "Username atau Password Salah", Toast.LENGTH_LONG).show();

        }else if (nama.isEmpty() && word.isEmpty()){
            Toast.makeText(this, "Isi dulu username dan passwordnya", Toast.LENGTH_LONG).show();
        }
    }
}
